from django.conf.urls import url
from views import UFListView, UFCalculatorView
urlpatterns = [
    url('list/$', UFListView.as_view()),
    url('price/', UFCalculatorView.as_view())
]