# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime

from rest_framework import  status
from rest_framework.views import APIView
from rest_framework.response import Response

from models import UF
from serializer import UFSerializer

class UFListView(APIView):
    def get(self, request, format=None):
        serializer = UFSerializer(UF.objects.all().order_by('date'), many=True)
        return Response(serializer.data)

class UFCalculatorView(APIView):
    def get(self, request, format=None):
        try:
            value = int(request.GET['value'] if 'value' in request.GET else None)
            date = request.GET['date'] if 'date' in request.GET else None
            if value and date:
                date_to = datetime.datetime.strptime(date, '%Y%m%d').date()
                uf = UF.objects.get(date=date_to)
                calc = int(round(uf.value*value,0))
                return Response(
                    {
                     'result': calc,
                     'currency':'CLP'
                    })
            else:
                return Response(
                    data={'message':'Falta uno de los parametros de entrada'},
                    status=status.HTTP_400_BAD_REQUEST
                )
        except:
            return Response(
                data={'message':'Formato incorrecto en parametros de entrada'},
                status=status.HTTP_400_BAD_REQUEST
            )