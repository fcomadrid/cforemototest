**Instalation**

    1.- Clone repository 'git clone https://fcomadrid@bitbucket.org/fcomadrid/cforemototest.git'
    2.- Create a new environment for python 2.7
    3.- In the enviroment install the dependencies with 'pip install -r requirements.txt'
    4.- Create the model in the db.sqlite with 'python manage.py migrate'
    5.- For populate the table with the historic UF 'python loaddata.py' and wait to the message "Aplicacion lista para ser usada"
    6.- Start the application with 'python manage.py runserver'
    7.- You can access to the url http://127.0.0.1:8000/uf/ list & price.
