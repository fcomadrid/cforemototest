from datetime import datetime, date
import urllib
import urllib2
import os

import django
import xlrd
from django.db import transaction

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "CFORemoto.settings")
django.setup()

from AppUF.models import UF

url = 'http://si3.bcentral.cl/estadisticas/Principal1/Excel/UF_IVP_CRM/UF_IVP.xls'
path = 'upload'
filename = 'UF-{:%d%m%Y%H%M%S}.xls'.format(datetime.now())
fullpath = '%s/%s' %(path,filename)

urllib.urlretrieve(url, fullpath)

urllib2.urlopen(url)

doc = xlrd.open_workbook(fullpath)

uf_sheet = doc.sheet_by_index(0)

nrows = uf_sheet.nrows - 2
ncols = uf_sheet.ncols
year = 0
with transaction.atomic():
    for i in range(8,nrows):
        for j in range(2,ncols):
            if uf_sheet.cell(i, 0).value:
                year = float(str(uf_sheet.cell(i, 0).value).replace('(1)',''))
            day = uf_sheet.cell(i,1).value
            if uf_sheet.cell(i,j).value:
                model = UF()
                model.date = date(int(year),int(j-1),int(day))
                model.value = uf_sheet.cell(i,j).value
                model.save()

print "Aplicacion lista para ser usada"
