Django==1.11.6
djangorestframework==3.7.0
et-xmlfile==1.0.1
jdcal==1.3
pytz==2017.2
xlrd==1.1.0
